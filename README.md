# TwinPy PLC GUI #

Basic usage of the TwinPy library with a TwinCAT 3 PLC project. 

See TwinPy: https://bitbucket.org/ctw-bw/twinpy

## Install ##

For more details, find the TwinPy ReadMe. The getting-started steps are largely applicable here too. 

1. Clone repo recursively:  
`git clone <url> --recursive`
   * You can also clone regularly and then pull the submodules:  
    `git submodules update --init`
2. Open a terminal inside the `GUI/` directory
3. Create a virtual environment and activate it:  
`python -m venv venv`  
`venv\Scripts\activate.ps1`
4. Install the requirements and the TwinPy submodule:  
`pip install -r requirements.txt`  
`pip install -e twinpy`
5. Open the TwinCAT solution: `TcProject\TcProject.sln`
6. Activate configuration, login the PLC and start the PLC
7. Now run the GUI:  
`python main.py`
8. Make changes in the settings and watch the counter change behaviour. You can also change the values in the online view of the PLC from TwinCAT in Visual Studio. Note how the GUI updates the changes made elsewhere.
