from PyQt5.QtWidgets import (
    QWidget,
    QLabel,
    QVBoxLayout,
    QHBoxLayout,
    QFormLayout,
    QGroupBox,
)
from typing import Dict

from twinpy.ui import TcMainWindow
from twinpy.twincat import TwincatConnection, Symbol, Signal, Parameter
from twinpy.ui import TcLineEdit, TcLabel


class MyPlcGUI(TcMainWindow):
    """GUI for demo PLC"""

    def __init__(self, plc: TwincatConnection):

        self.plc = plc

        super().__init__()  # Parent constructor

        main_widget = QWidget()  # Widget spanning the entire window
        self.layout_main = QVBoxLayout(main_widget)  # Main layout
        self.setCentralWidget(main_widget)  # Make widget the main thingy

        # In this dict we will store all symbol instances
        self.symbols: Dict[str, Symbol] = {
            'counter': Signal(plc=self.plc, name='MAIN.counter'),
            'increment': Parameter(plc=self.plc, name='MAIN.increment'),
            'my_double': Parameter(plc=self.plc, name='MAIN.my_double'),
        }

        # ------- Input boxes -------

        self.input_increment = TcLineEdit(
            symbol=self.symbols['increment'],
            format='%d'
        )
        self.input_double = TcLineEdit(symbol=self.symbols['my_double'])

        group_input = QGroupBox("Input")
        layout_group_input = QFormLayout(group_input)  # Create layout for group

        # Now add widgets to layout:
        layout_group_input.addRow(QLabel("Increment"), self.input_increment)
        layout_group_input.addRow(QLabel("MyDouble"), self.input_double)

        self.layout_main.addWidget(group_input)

        # ------- Labels -------

        self.label_counter = TcLabel(
            symbol=self.symbols['counter'],
            format='%d'
        )

        group_output = QGroupBox("Output")
        layout_group_output = QFormLayout(group_output)  # Create layout for group

        # Now add widgets to layout:
        layout_group_output.addRow(QLabel("Counter"), self.label_counter)

        self.layout_main.addWidget(group_output)

        # -------

        self.show()  # Windows is hidden by default
