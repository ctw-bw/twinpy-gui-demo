from PyQt5.QtWidgets import QApplication
import sys
from pyads import ADSError

from twinpy.twincat import TwincatConnection

from gui import MyPlcGUI


def main():
    """Main function - used to keep global variable space clean"""

    app = QApplication(sys.argv)

    try:
        connection = TwincatConnection(ams_net_port=851)

    except ADSError as err:
        connection = None
        print("Not connected to TwinCAT, continuing -", err)

    gui = MyPlcGUI(connection)

    app.exec()


if __name__ == "__main__":
    main()
